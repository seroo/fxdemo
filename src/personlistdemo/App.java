package personlistdemo;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class App extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	protected boolean isLogin = false;
	protected Scene scene;
	Stage secondaryStage;
	Stage addCustomerStage = new Stage();;
	Scene scene2;
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		
// Login Page
		DisplayLoginPage loginPage = new DisplayLoginPage();
		loginPage.isButtonClicked.addListener(new ChangeListener<Boolean> () {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {

				changeStage(scene2, primaryStage);
				
			}
			
		});
		
// Customer List Page

		BorderPane borderPane = new BorderPane();
		TableView tableView = new TableView();
		HBox hBox = new HBox();
		Button buttonAddCustomer = new Button("Add Customer");
		buttonAddCustomer.setOnAction(new EventHandler<ActionEvent> () {

			@Override
			public void handle(ActionEvent event) {

				AddCustomer addCustomer = new AddCustomer(addCustomerStage);
				try {
					addCustomer.start(addCustomerStage);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
			
		});
				
				
				
				
		//*/////
		hBox.getChildren().add(buttonAddCustomer);
		
		hBox.setMargin(buttonAddCustomer, new Insets(8, 8, 8, 0));
		hBox.setAlignment(Pos.BASELINE_RIGHT);

		// Tableview icerigi
		ObservableList<Customer> data = FXCollections.observableArrayList(new Customer("John", "jhon@jhon", "321321"),
				new Customer("Jane", "jane@jhon", "6546543"));
		tableView.setItems(data);

		TableColumn nameColumn = new TableColumn("First Name");
		// en sondaki string objenin property degiskeni ile ayni ad olmali yoksa tablda
		// gozukmuyor
		nameColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("name"));
		TableColumn mailColumn = new TableColumn("Email");
		mailColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("mail"));
		TableColumn phoneColumn = new TableColumn("Phone Number");
		phoneColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("phoneNumber"));

		tableView.getColumns().addAll(nameColumn, mailColumn, phoneColumn);

		borderPane.setTop(hBox);
		borderPane.setCenter(tableView);
		
		scene2 = new Scene(borderPane, 300, 200);
		
		tableView.setPrefHeight(scene2.getHeight());
		tableView.setPrefWidth(scene2.getWidth());

//		System.out.println(scene2.getHeight());
		
		
		primaryStage.setTitle("Login");
		primaryStage.setResizable(false);
		primaryStage.setScene(loginPage.getScene());
		primaryStage.show();
	}
	
	private void changeStage(Scene scene2, Stage primaryStage) {
		
		primaryStage.setScene(scene2);
		primaryStage.show();
		
		primaryStage.setTitle("Cok gizli musteri listesi");
	}
}
