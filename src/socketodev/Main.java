package socketodev;


public class Main {

	public static void main(String[] args) {

		EchoClient client = new EchoClient();
		
		client.startConnection("localhost", 5555);
		
		client.sendMessage("Seroo");
		
		client.stopConnection();
		
	}

}
