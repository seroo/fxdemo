
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;


public class LiangFx extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {

		Button buttonOK = new Button("OK");
		
		Scene scene = new Scene(buttonOK, 200, 250);
		
		primaryStage.setTitle("Liang JavaFX");
		
		primaryStage.setScene(scene);
		primaryStage.show();
		
	}

	public static void main(String[] args) {
		Application.launch(args);
	}
}



