import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class ShowCircle extends Application{

	@Override
	public void start(Stage primaryStage) throws Exception {

		Circle circle = new Circle();
		circle.setCenterX(100);
		circle.setCenterY(100);
		circle.setRadius(50);
		
		circle.setStroke(Color.BLACK);
		circle.setFill(Color.WHITE);
		
		// sadece pane yap�nca label i ortalam�yor // Stackpane herşeyi ortaya üstüste koyarmış
		Pane pane = new StackPane();
		pane.getChildren().add(circle);
		
		Label label = new Label("Java FX");
		label.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.ITALIC, 20));
		
		pane.getChildren().add(label);
		
		Scene scene = new Scene(pane, 200, 200);
		System.out.println(scene.getHeight() + " " + scene.getWidth());
		primaryStage.setScene(scene);
		
		primaryStage.show();
		
	}
	
	
	
	
	public static void main(String[] args) {

		launch();
		
		
	}

	

}
