package hafta8yedek;
import java.sql.*;

public class JdbcApp1 {

	public static void main(String[] args) throws SQLException {
		// APACHE DERBY driver ı in memory yapıda program kapanınca db siliniyor.
		//Connection dbConn = DriverManager.getConnection("jdbc:derby:memory:mydb1;create=true");
		/*
		 * dosyaya yazılan versiyonu
		 */
		//Connection dbConn = DriverManager.getConnection("jdbc:derby:./mydb1/;create=true");
		
		// bi de server/client mantığı ile çalışanı var ama gereksiz - ayrı bir derby client dependency si var
		
		/*
		 * MYSQL bağlantısı
		 */
		Connection dbConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb1", "root", "root");
		// Transaction managment
		dbConn.setAutoCommit(false);
		
		Statement statement = dbConn.createStatement();
		
		statement.executeUpdate("CREATE TABLE IF NOT EXISTS students (id int primary key, name varchar(30)) ");
//		
//		statement.executeUpdate("INSERT INTO students VALUES(1, 'onur')");
//		statement.executeUpdate("INSERT INTO students VALUES(2, 'koray')");
//		statement.executeUpdate("INSERT INTO students VALUES(3, 'ozkan')");
//		
		
//		int id = 7; // frontend ten geldiğini düşünelim
//		String name = "Tom";
		
		/*
		 * önce kommit edip hata almazsak database e gönderiyor eğer hata verirse rollback ile tüm işlemi geri alıyor 
		 * Mesela ödeme yapılamazsa satın alma işlemindeki kayıtları geri al gibi
		 */
		try {
			createStudent(dbConn, 21, "İsim 1");
			createStudent(dbConn, 22, "İsim 2");
			createStudent(dbConn, 17, "İsim 3");
			dbConn.commit();
		} catch (Exception e) {
			System.out.println("Error Transaction: " + e.toString());
			dbConn.rollback();
			e.printStackTrace();
		}
		
		
		ResultSet rs = statement.executeQuery("select id, name from students");
		
		while(rs.next()) {
			System.out.printf("%d\t%s\n", rs.getInt("id"), rs.getString("name"));
		}
		dbConn.close();
	}

	private static void createStudent(Connection dbConn, int id, String name) throws SQLException {
		PreparedStatement stmtPrep = dbConn.prepareStatement("INSERT INTO students VALUES(?, ?)");
		stmtPrep.setInt(1, id);
		stmtPrep.setString(2, name);
		stmtPrep.executeUpdate();
	}

}
