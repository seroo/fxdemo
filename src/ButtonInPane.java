import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class ButtonInPane extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {

		StackPane pane = new StackPane();
		Button button = new Button("OK");
//		button.setRotate(60);
		pane.getChildren().add(button);
		Scene scene = new Scene(pane, 200, 60);
		primaryStage.setTitle("Button in Pane");
		primaryStage.setScene(scene);
		primaryStage.show();
		
	}

	public static void main(String[] args) {
		launch(args);
	}
	
}
