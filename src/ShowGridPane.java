
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

//package.import ShowGridPane.java;

public class ShowGridPane extends Application {

	@Override
	public void start(final Stage primaryStage) throws Exception {

		GridPane pane = new GridPane();

		pane.setAlignment(Pos.CENTER);
		pane.setPadding(new Insets(12, 8, 12, 8));
		pane.setHgap(8);
		pane.setVgap(8);

		pane.add(new Label("First Name"), 0, 0);
		pane.add(new TextField(), 1, 0);
		pane.add(new Label("MI"), 0, 1);
		pane.add(new TextField(), 1, 1);
		pane.add(new Label("Last Name"), 0, 2);
		pane.add(new TextField(), 1, 2);
		Button buttonAdd = new Button("Add Name");
		buttonAdd.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				ShowFlowPane showGrid = new ShowFlowPane();
				primaryStage.setScene(showGrid.scene);
				
			}
			
			
			
//			@Override
//			public void handle(ActionEvent event) {
//
//				Stage newStage = new Stage();
//				ShowGridPane showGrid = new ShowGridPane();
//				try {
//					showGrid.start(newStage);
//				} catch (Exception e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				primaryStage.close();
//
//			}
		});
		GridPane.setHalignment(buttonAdd, HPos.RIGHT);
		
//		HBox pane2 = new HBox();
//		pane2.setAlignment(Pos.BASELINE_RIGHT);
		pane.add(buttonAdd, 1, 3);
		
		Scene scene = new Scene(pane);
		
		primaryStage.setScene(scene);

		primaryStage.show();
		
		
		
	}
	
	public static void main(String[] args) {
		launch();
	}

}
