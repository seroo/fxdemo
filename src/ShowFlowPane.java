
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class ShowFlowPane extends Application {

	FlowPane pane;
	TextField textFieldMI;
//	TextField textFieldFirstName;
//	TextField textFieldLastName;
//	Label firstName;
//	Label lastName;
//	Label MI;
	Scene scene;
//	
	ShowFlowPane() {
		pane.setPadding(new Insets(12, 8, 12, 8));
		pane.setHgap(8);
		pane.setVgap(8);
		textFieldMI = new TextField();
		textFieldMI.setPrefColumnCount(1);
		pane.getChildren().addAll(new Label("First Name"), new TextField(), new Label("MI:"),
				textFieldMI, new Label("Last Name"), new TextField());
		
		scene = new Scene(pane, 200, 250);
	}
	
	
	
	@Override
	public void start(Stage primaryStage) throws Exception {

		FlowPane pane = new FlowPane();
		
		pane.setPadding(new Insets(12, 8, 12, 8));
		pane.setHgap(8);
		pane.setVgap(8);
		TextField textFieldMI = new TextField();
		textFieldMI.setPrefColumnCount(1);
		pane.getChildren().addAll(new Label("First Name"), new TextField(), new Label("MI:"),
				textFieldMI, new Label("Last Name"), new TextField());
		
		Scene scene = new Scene(pane, 200, 250);
		
		primaryStage.setScene(scene);

		primaryStage.show();
		
		
		
	}
	
//	public static void main(String[] args) {
//		launch();
//	}

}
